import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'birthday-sort';
  shuffled = true;

  log:any=()=>{
    console.log("it works");
  }

  parentCall:any=()=>{
    this.shuffled = !this.shuffled;
  }
}
