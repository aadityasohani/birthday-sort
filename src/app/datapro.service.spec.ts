import { TestBed } from '@angular/core/testing';

import { DataproService } from './datapro.service';

describe('DataproService', () => {
  let service: DataproService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataproService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
