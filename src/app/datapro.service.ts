import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DataproService {
  url:string="";

  constructor(private http:HttpClient) { 
    this.url="https://bitbucket.org/aadityasohani/birthday-sort/raw/863a53f8b643f8972a9793b4b1945dc7e9377154/birthday.json";
  }

  getData(){
    return this.http.get(this.url);
  }
}
