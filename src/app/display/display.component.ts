import { Component, OnInit, Input} from '@angular/core';
import { DataproService } from '../datapro.service';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit{

  sortingType:String= "random";
  @Input() shuffled:boolean=true;
  @Input() call:any;

  constructor(private dataProService:DataproService) { }
  data:any;
  ngOnInit(): void {
    this.dataProService.getData().subscribe((data)=>{
      this.data = data;
    })
  }

  ngOnChanges(shuffled:boolean):void{
    // for (let i = this.data.length - 1; i > 0; i--) {
    //   let j = Math.floor(Math.random() * (i + 1)); // random index from 0 to i
  
    //   [this.data[i], this.data[j]] = [this.data[j], this.data[i]];
    // }
    if(this.data){

    this.data.sort(()=>Math.random()-0.5);
    this.sortingType="random";
    this.call();
    }
  }

  comparebyName(d1:any,d2:any):number{
    if(d1.name<d2.name){
      return -1;
    }
    return 1;
  }
  comparebyNameDesc(d1:any,d2:any):number{
    if(d1.name>=d2.name){
      return -1;
    }
    return 1;
  }
  comparebyDob(d1:any,d2:any):number{
    var dt1= new Date(d1.dob.slice(6)+"/"+d1.dob.slice(3,5)+"/"+d1.dob.slice(0,2));
    var dt2= new Date(d2.dob.slice(6)+"/"+d2.dob.slice(3,5)+"/"+d2.dob.slice(0,2));
    if(dt1<=dt2){
      return 1;
    }else{
      return -1;
    }

    //dd-mm-yyyy to ---->>>> yyyy-mm-dd
  }
  comparebyDobDec(d1:any,d2:any):number{
    var dt1= new Date(d1.dob.slice(6)+"/"+d1.dob.slice(3,5)+"/"+d1.dob.slice(0,2));
    var dt2= new Date(d2.dob.slice(6)+"/"+d2.dob.slice(3,5)+"/"+d2.dob.slice(0,2));
    if(dt1>=dt2){
      return 1;
    }else{
      return -1;
    }

    //dd-mm-yyyy to ---->>>> yyyy-mm-dd
  }


  sortByName(a:boolean):void{
    if(a){this.data.sort(this.comparebyName);this.sortingType="name (ascending)";}
    else {this.data.sort(this.comparebyNameDesc);this.sortingType="name (descending)";}
  }

  sortByDob(a:boolean):void{
    if(a){this.data.sort(this.comparebyDob);this.sortingType="dob (ascending)";}
    else {this.data.sort(this.comparebyDobDec);this.sortingType="dob (descending)";}
  }

}
