import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RandomBottonComponent } from './random-botton.component';

describe('RandomBottonComponent', () => {
  let component: RandomBottonComponent;
  let fixture: ComponentFixture<RandomBottonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RandomBottonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RandomBottonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
