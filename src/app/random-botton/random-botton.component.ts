import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-random-botton',
  templateUrl: './random-botton.component.html',
  styleUrls: ['./random-botton.component.css']
})
export class RandomBottonComponent implements OnInit {

  constructor() { }
  @Output() emitEvent:EventEmitter<any> = new EventEmitter<any>();

  ngOnInit(): void {
  }

  onClick = ()=>{
    this.emitEvent.emit();
  }

}
